<?php

/**
 * @file
 * Provide views data and other hooks.
 */

/**
 * Implements hook_views_data().
 */
function block_pane_views_data() {
  $data['views']['block_pane'] = array(
    'title' => t('Block pane'),
    'help' => t('Adds the chosen block.'),
    'area' => array(
      'handler' => 'views_handler_area_block_pane',
    ),
  );
  return $data;
}
