<?php

/**
 * @file
 * Custom Block textgroup handler
 */

class block_pane_block_object extends i18n_block_object {
  /**
   * Get object strings for translation.
   */
  protected function build_properties() {
    // Get block i18n defined properties.
    $properties = parent::build_properties();

    // If current block is provided by our module - make body string translatable.
    if ('block_pane' == $this->object->module) {
      // Get original properties of our block body.
      $original_properties = i18n_string_object_wrapper::build_properties();
      // Get aliaces.
      $delta = $this->object->delta;
      $textgroup = $this->get_textgroup();
      // Define field for block body as translatable.
      $properties[$textgroup]['block_pane'][$delta]['body'] = $original_properties[$textgroup]['block_pane'][$delta]['body'];
      // Fill default values.
      $block = block_pane_load($delta);
      $properties[$textgroup]['block_pane'][$delta]['body']['string'] = $block['body'];
      $properties[$textgroup]['block_pane'][$delta]['body']['format'] = $block['format'];
    }
    return $properties;
  }
}
