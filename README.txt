/**
 * What Block pane module does?
 */
Block pane module provides a better integration of drupal default blocks with panels.
Blocks that are created by this module have a unique delta (machine name) what will make
your deployment with features easier. As well there are few additional tools
for views and panels, which will make development more comfortable.

What you will have with Block pane module?
-Blocks which looks like drupal default blocks, but they have more stable functionality.
-These blocks are not binded to the bid (block id), they have a machine names.
-Support for multilingual translations.
-Integration with features. (You can use Features extra for blocks settings export.)
-You can easily insert your blocks into views
-And also you have additional tools, which will make your work with CTools panels more comfortable.

This module is useful, if your site pages will builded with CTools panels. In other cases you could do without Block pane. I hope you'll enjoy it.

/**
 * How to use Block pane module?
 */
- To create a blocks, you can visit "admin/structure/block/add-block-pane" page.
- To edit blocks you can follow by link "Edit" on the blocks list page "admin/structure/block".
- To insert blocks into views, you need to use "Global: Block pane" handler.
- To insert blocks into panels, you need to go to the panel management page and press "Add content" button. After that:
  1. You can create new blocks by pressing "Block pane" link in the left side of the screen.
  2. Or you can insert a block which is already exist. Just press "Block panes" group and chose a block.
- To make your blocks with support of multilingual translations, you need to enable block_pane_multilingual module (and install all it's dependencies).
- To use features for blocks, you need to enable block_pane_features module (and install all it's dependencies)
