<?php

/**
 * @file
 * Functionality for administration pages (forms etc.).
 */

/**
 * Function to build a form which can define blocks (Page: "admin/structure/block/add-block-pane").
 */
function block_pane_add_form($form, &$form_state) {
  // Block form.
  $form += _block_pane_plugin_block_create_form();
  return $form;
}

/**
 * Submit function (Form: "block_pane_add_form").
 */
function block_pane_add_form_submit($form, &$form_state) {
  // Receive delta and save it.
  $delta = $form_state['values']['new_delta'];
  $block = array(
    'delta' => $delta,
    'info' => $form_state['values']['info'],
  );

  // Block creation.
  block_pane_save($block);
  $form_state['redirect'] = format_string('admin/structure/block/manage/block_pane/!delta/configure', array('!delta' => $delta));
}

/**
 * Function to build a form which can delete blocks (Page: "admin/structure/block/manage/block-pane/%/delete").
 */
function block_pane_delete_form($form, &$form_state, $delta) {
  $block = block_pane_load($delta);
  $info = isset($block['info']) ? $block['info'] : '<none>';
  $form['delta'] = array(
    '#type'  => 'value',
    '#value' => $delta,
  );
  $form['warning'] = array('#markup' => t('This action cannot be undone.'));
  return confirm_form($form, t('Are you sure you want to delete the block %name?', array('%name' => $info)), 'admin/structure/block', '', t('Delete'), t('Cancel'), 'confirm');
}

/**
 * Submit function (Form: "block_pane_delete_form").
 */
function block_pane_delete_form_submit($form, &$form_state) {
  block_pane_delete($form_state['values']['delta']);
  $form_state['redirect'] = 'admin/structure/block';
}

/**
 * Provide a form for a block creation.
 */
function _block_pane_plugin_block_create_form() {
  $form = array();
  // Block info.
  $form['info'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Block description'),
    '#maxlength'     => 64,
    '#description'   => t('A brief description of your block. Used on the <a href="@overview">Blocks administration page</a>.', array('@overview' => url('admin/structure/block'))),
    '#required'      => TRUE,
  );
  // New block delta.
  $form['new_delta'] = array(
    '#type'             => 'machine_name',
    '#title'            => t('Block delta'),
    '#maxlength'        => 32,
    '#element_validate' => array('block_pane_delta_validate_element'),
    '#machine_name'     => array(
      'source' => array('info'),
    ),
  );
  // Save and continue.
  $form['buttons']['next'] = array(
    '#type'        => 'submit',
    '#value'       => t('Save and continue'),
    '#wizard type' => 'next',
    '#id'          => 'block-pane-save-and-continue',
  );
  return $form;
}

/**
 * Provide a form for block editing.
 */
function _block_pane_plugin_block_edit_form($delta) {
  $form = array();
  $block = block_pane_load($delta);
  if (isset($block)) {
    $form['warning'] = array(
      '#type'       => 'html_tag',
      '#tag'        => 'b',
      '#value'      => t('Remember! You trying to edit block settings directly, not the pane instance.'),
    );
  }
  // Block info.
  $form['info'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Block description'),
    '#maxlength'     => 64,
    '#description'   => t('A brief description of your block. Used on the <a href="@overview">Blocks administration page</a>.', array('@overview' => url('admin/structure/block'))),
    '#required'      => TRUE,
    '#default_value' => isset($block['info']) ? $block['info'] : '',
  );
  // Block title.
  $form['title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Block title'),
    '#maxlength'     => 64,
    '#description'   => t('Override the default title for the block. Use <none> to display no title, or leave blank to use the default block title.'),
    '#default_value' => isset($block['title']) ? $block['title'] : '',
  );
  // Block body.
  $form['body'] = array(
    '#type'          => 'textfield',
    '#type'          => 'text_format',
    '#title'         => t('Block body'),
    '#rows'          => 15,
    '#description'   => t('The content of the block as shown to the user.'),
    '#required'      => 1,
    '#default_value' => isset($block['body']) ? $block['body'] : '',
  );
  if (isset($block['format'])) {
    $form['body']['#format'] = $block['format'];
  }
  return $form;
}

/**
 * Provide a form for block delta.
 */
function _block_pane_plugin_block_delta_form($delta) {
  $form = array();
  // Possibility to change block delta.
  $form['new_delta'] = array(
    '#type'             => 'machine_name',
    '#title'            => t('Block delta'),
    '#maxlength'        => 32,
    '#default_value'    => $delta,
    '#weight'           => -17.5,
    '#element_validate' => array('block_pane_delta_validate_element'),
    '#machine_name'     => array(
      'source' => array('info'),
    ),
  );

  // Warning for a user.
  $warning = array(
    '#type' => 'html_tag',
    '#tag' => 'b',
    '#value' => t('Changing of block delta can cause error "Deleted/missing block" in panels or remove your translations and other settings.'),
  );
  $form['new_delta']['#description'] = t('A unique machine-readable name. Can only contain lowercase letters, numbers, and underscores.') . '<br/>' . drupal_render($warning);
  return $form;
}
