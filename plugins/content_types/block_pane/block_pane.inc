<?php

/**
 * @file
 * Provide block pane in panels.
 */

/**
 * Description of our plugin.
 */
$plugin = array(
  'title' => t('Block pane'),
);

/**
 * Block pane content types generation.
 */
function block_pane_block_pane_content_type_content_types() {
  // Loading of blocks list.
  $blocks_list = block_pane_list();
  $blocks = block_pane_load_multiple(array_keys($blocks_list));
  // Content type for block creation.
  $content_types = array(
    'block_pane' => array(
      'title' => t('Block pane'),
      'defaults' => array(
        'default_delta' => NULL,
      ),
      'description' => t('Add new block pane as content.'),
      'category' => t('Block pane'),
      'top level' => TRUE,
    ),
  );
  // Content types of existing blocks.
  foreach ($blocks as $delta => $data) {
    $content_types['block_pane-' . $delta] = array(
      'title' => "[$delta] " . $data['info'],
      'category' => t('Block panes'),
      'defaults' => array(
        'default_delta' => $delta,
      ),
    );
  }
  return $content_types;
}

/**
 * Block admin info in panels.
 */
function block_pane_block_pane_content_type_admin_info($subtype, $conf) {
  // Get necessary data.
  $delta = $conf['delta'];
  $block = block_pane_load($delta);
  if (!empty($block)) {
    $block = (object) $block;
    // Sanitize the block because <script> tags can hose javascript up:
    if (!empty($block->body)) {
      $block->content = filter_xss_admin(render($block->body));
    }
    // Block title.
    if (empty($block->title) || $block->title == '<none>') {
      $block->title = t('No title');
    }
    // Remove unwanted keys.
    unset($block->format, $block->info, $block->delta, $block->body);
    // Return data.
    return $block;
  }
}

/**
 * Returns the administrative title for a type.
 */
function block_pane_block_pane_content_type_admin_title($subtype, $conf) {
  // Fetch necessary data.
  $delta = $conf['delta'];
  if ($block = block_pane_load($delta)) {
    // Block admin title.
    return t('Block: !title', array('!title' => check_plain($block['info'])));
  }
  // If we can't obtain a block.
  return t('Deleted/missing block @module-@delta', array('@module' => 'block_pane', '@delta' => $delta));
}

/**
 * Edit form for blocks.
 */
function block_pane_block_pane_content_type_edit_form($form, &$form_state) {
  // Find the block delta.
  if (isset($form_state['conf']['delta'])) {
    $delta = $form_state['conf']['delta'];
    // Validate block delta.
    if (!block_pane_load($delta)) {
      $form = array();
      $form['warning']['#markup'] = t('Deleted/missing block @module-@delta', array('@module' => 'block_pane', '@delta' => $delta));
      return $form;
    }
  }
  else {
    $delta = $form_state['conf']['default_delta'];
  }
  // Check the possibility of user to edit blocks.
  if (user_access('administer blocks')) {
    // Load file with forms.
    module_load_include('inc', 'block_pane', 'block_pane.admin');
    if (!isset($delta)) {
      // Load form to create a block if we need.
      $form += _block_pane_plugin_block_create_form();
    }
    // Form to edit block.
    $form += _block_pane_plugin_block_edit_form($delta);
  }
  return $form;
}

/**
 * Submit function (for form: "block_pane_block_pane_content_type_edit_form").
 */
function block_pane_block_pane_content_type_edit_form_submit($form, &$form_state) {
  // Get aliaces.
  $conf = &$form_state['conf'];
  $values = $form_state['values'];
  // Prepare our block.
  $block = array(
    'info' => $values['info'],
    'title' => $values['title'],
    'body' => $values['body']['value'],
    'format' => $values['body']['format'],
  );
  // Get block delta.
  if (empty($conf['delta'])) {
    $conf['delta'] = isset($values['new_delta']) ? $values['new_delta'] : $conf['default_delta'];
  }
  $block['delta'] = $conf['delta'];
  // Save our block.
  block_pane_save($block);
}

/**
 * Function to render block content.
 */
function block_pane_block_pane_content_type_render($subtype, $conf, $args, $context) {
  // Fetch necessary data.
  $delta = $conf['delta'];
  if (!($info = _block_pane_blocks_info('block_pane', $delta))) {
    return;
  }
  // Call our block and let other modules to alter it.
  $block = module_invoke('block_pane', 'block_view', $delta);
  drupal_alter(array('block_view', "block_view_block_pane_{$delta}"), $block, $info);

  // Make correct values.
  $block = (object) $block;
  $block->module = 'block_pane';
  $block->delta = $delta;
  if (!isset($block->title)) {
    $block->title = $block->subject;
  }
  unset($block->subject);
  // Contextual links.
  if (user_access('administer blocks')) {
    $block->admin_links = array(
      array(
        'title' => t('Configure block'),
        'href' => "admin/structure/block/manage/block_pane/$delta/configure",
        'query' => drupal_get_destination(),
      ),
    );
  }
  return $block;
}
