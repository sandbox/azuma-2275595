<?php

/**
 * @file
 * Definition of views_handler_area_block_pane.
 */

/**
 * Views area block pane handler.
 */
class views_handler_area_block_pane extends views_handler_area {

  /**
   * Options definition.
   */
  function option_definition() {
    $options = parent::option_definition();
    // Define new option.
    $options['block_pane'] = array('default' => NULL);
    return $options;
  }

  /**
   * Handler options form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Gather the blocks defined by modules.
    $blocks = array();
    foreach (module_implements('block_info') as $module) {
      $module_blocks = module_invoke($module, 'block_info');
      foreach ($module_blocks as $delta => $block) {
        $blocks[$module]["$module|-|$delta"] = $block['info'];
      }
    }

    // Show select list with blocks.
    $form['block_pane'] = array(
      '#title'         => t('Chose block'),
      '#type'          => 'select',
      '#options'       => $blocks,
      '#required'      => TRUE,
      '#default_value' => $this->options['block_pane'],
    );
  }

  /**
   * Method created in order to render the block.
   */
  function render($empty = FALSE) {
    // Obtain block module and delta.
    list($module, $delta) = explode('|-|', $this->options['block_pane']);

    if ($info = _block_pane_blocks_info($module, $delta)) {
      // Define block.
      $block = new stdClass();
      // Fill block with data.
      $block->bid = $info->bid;
      $block->module = $module;
      $block->delta = $delta;
      $block->region = BLOCK_REGION_NONE;
      // Get block title.
      $block->title = isset($info->title) ? $info->title : NULL;

      // Render our block.
      $block = _block_render_blocks(array($block));
      $block = _block_get_renderable_array($block);
      return render($block);
    }
    return '';
  }
}
